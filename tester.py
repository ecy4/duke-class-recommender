import class_recommender

import time
import matplotlib.pyplot as plt
import collaborative_filtering as mvp
import matrix_factorization as ext1
import content_filtering as ext2


def populate_data(data, num_users, num_attributes):
    users = {}
    courses = {}
    data.readline()
    for line in data:
        line = line.split(',')
        userid = int(line[0])
        if line[1] == '1':
            isCourse = True
        else:
            isCourse = False
        attributeid = int(line[2])
        rating = int(line[3])

        if userid < num_users and (isCourse or attributeid < num_attributes):
            if userid not in users:
                users[userid] = class_recommender.User(userid, {}, {})
                for i in range(18,86):
                    users[userid].factorizedCourseRatings[i] = 0

            if isCourse:
                users[userid].add_course_rating(attributeid, rating)
                courses[attributeid] = class_recommender.Course(attributeid, "")
            else:
                users[userid].add_preference(attributeid, rating)

    return users, courses

def run_analysis(function, function_name):
    output = open("runtime_results_" + function_name + ".csv", "w")
    for i in range(18, 0, -1):
        times = []
        output.write("%d," % i)
        for N in range(1000, 10001, 1000):
            users = {}
            data = open("test_data.csv", "r")
            users, courses = populate_data(data, N, i)

            start = time.time()
            if function_name == "extension2":
                _results = function(courses, users, 1, range(i), False)
            else:
                _results = function(users, courses, 1, range(i), False)
            end = time.time()
            print("Runtime for i=%d, N=%d: %.03f" % (i, N, end - start))
            times.append(end - start)
            output.write("%.06f," % (end - start))
            data.close()
        output.write("\n")
        plt.plot(range(1000, 10001, 1000), times, label="i=" + str(i))
    output.close()
    plt.ylabel('Runtime (seconds)')
    plt.xlabel('Number of users')
    plt.title("Class Recommender runtimes (" + function_name + ") for varying preference attributes count (i)", wrap=True)
    plt.legend(bbox_to_anchor=(1.04, -0.04), loc="lower left")
    plt.savefig("figures/runtime_analysis_" + function_name, bbox_inches="tight")
    plt.show()


if __name__ == "__main__":
    run_analysis(mvp.getPredictedCourseRatings, "mvp")
    run_analysis(ext1.getPredictedCourseRatings, "extension1")
    run_analysis(ext2.getPredictedCourseRatingsApproach1, "extension2")